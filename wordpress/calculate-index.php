<?php

function city_names() {
  global $livability;
  $names = array();
  foreach($livability['city'] as $key => $value) {
    $names[] = $key;
  }
  return $names;
}

function category_names() {
  global $livability;
  $names = array();
  foreach($livability['indicators'] as $key => $value) {
    $name = $value['category'];
    if (!is_null($name) && !in_array($name, $names)) {
      $names[] = $name;
    }
  }
  sort($names);
  return $names;
}

function category_indicator_names($category) {
  global $livability;
  $names = array();
  foreach($livability['indicators'] as $key => $value) {
    if ($value['category'] == $category && is_null($value['subcategory'])) {
      $name = $key;
      if (!is_null($name) && !in_array($name, $names)) {
        $names[] = $name;
      }
    }
  }
  sort($names);
  return $names;
}

function subcategory_indicator_names($subcategory) {
  global $livability;
  $names = array();
  foreach($livability['indicators'] as $key => $value) {
    if ($value['subcategory'] == $subcategory) {
      $name = $key;
      if (!is_null($name) && !in_array($name, $names)) {
        $names[] = $name;
      }
    }
  }
  sort($names);
  return $names;
}

function subcategory_names($category) {
  global $livability;
  $names = array();
  foreach($livability['indicators'] as $key => $value) {
    $name = $value['subcategory'];
    if ($value['category'] == $category && !is_null($name) && !in_array($name, $names)) {
      $names[] = $name;
    }
  }
  sort($names);
  return $names;
}



function calculate_index($cities, $categories) {
  $ranks = array();
  $by_category = array();
  foreach($categories as $category) {
    $rank = calculate_category_index($cities, $category);
    $by_category[$category] = $rank;
    $ranks[] = $rank;
  }
  $combined = combine_ranks($ranks);
  $combined['basis'] = 'root';
  $combined['by_category'] = $by_category;
  return $combined;
}

function calculate_category_index($cities, $category) {
  global $livability;
  $ranks = array();
  $by_indicator = array();
  $by_subcategory = array();
  foreach(category_indicator_names($category) as $indicator) {
    $rank = calculate_indicator_index($cities, $indicator);
    $ranks[] = $rank;
    $by_indicator[$indicator] = $rank;
  }
  foreach(subcategory_names($category) as $subcategory) {
    $rank = calculate_subcategory_index($cities, $subcategory);
    $ranks[] = $rank;
    $by_subcategory[$subcategory] = $rank;
  }
  $combined = combine_ranks($ranks);
  $combined['basis'] = 'category';
  $combined['category'] = $category;
  $combined['by_subcategory'] = $by_subcategory;
  $combined['by_indicator'] = $by_indicator;
  return $combined;
}

function calculate_subcategory_index($cities, $subcategory) {
  global $livability;
  $ranks = array();
  $by_indicator = array();
  foreach(subcategory_indicator_names($subcategory) as $indicator) {
    $rank = calculate_indicator_index($cities, $indicator);
    $ranks[] = $rank;
    $by_indicator[$indicator] = $rank;
  }
  $combined = combine_ranks($ranks);
  $combined['basis'] = 'subcategory';
  $combined['subcategory'] = $subcategory;
  $combined['by_indicator'] = $by_indicator;
  return $combined;
}

function calculate_indicator_index($cities, $indicator) {
  global $livability;
  $values = get_indicator_values($cities, $indicator);
  $sort_descending = ($livability['indicators'][$indicator]['order'] == 'max');
  $ranked = rank_values($values, $sort_descending, $indicator);
  return array(
    "cities" => $cities,
    "ranked" => $ranked,
    "basis" => "indicator",
    "indicator" => $indicator
  );
}

function combine_ranks($ranks) {
  $cities = $ranks[0]['cities'];
  $values = array();
  foreach ($cities as $city) {
    $sum = 0;
    foreach ($ranks as $r) {
      $sum += $r['ranked'][$city]['rank'] * 1.0;
    }
    $values[$city] = $sum / count($ranks);
  }
  $ranked = rank_values($values); // sort ascending
  foreach ($ranked as $city=>$i) {
    $notes = array();
    foreach($ranks as $r) {
      $notes = array_merge($notes, $r['ranked'][$city]['notes']);
    }
    $ranked[$city]['notes'] = $notes;
  }
  return array(
    "cities" => $cities,
    "ranked" => $ranked
  );
}

function get_indicator_values($cities, $indicator) {
  global $livability;
  $count = count($cities);
  $values = array();
  foreach($cities as $city) {
    $values[$city] = $livability['city'][$city]['data'][$indicator]['value'];
  }
  return $values;
}

function rank_values($values, $sort_descending = false, $indicator = null) {
  global $livability;
  if ($sort_descending) {
    arsort($values, SORT_NUMERIC);
  } else {
    asort($values, SORT_NUMERIC);
  }
  $count = count($values);
  $index = array();
  $i = 1;
  foreach($values as $city => $value) {
    $index[$i] = array( 'city' => $city, 'value' => $value, 'rank' => $i * 1.0, 'rankl' => $i);
    if (!is_null($indicator)) {
      $meta = $livability['city'][$city]['data'][$indicator]['meta'];
      if (!is_null($meta)) {
        $index[$i]['notes'] = array(array('indicator' => $indicator, 'note' => $meta));
      } else {
        $index[$i]['notes'] = array();
      }
    }
    $i++;
  }

  $distinct = 1;
  for($i = 2; $i <= $count; $i++) {
    if ($index[$i]['value'] == $index[$i-1]['value']) {
      $rankl = $index[$i-1]['rankl'];
      // rankl = 1, i = 3, rank = 2  (1+3)/2 = 4/2 = 2
      // rankl = 1, i=4, rank=2.3  (1+4)/2 = 5/2 = 2.5
      $rank = ($rankl + $i) / 2.0;
      for($j = $rankl; $j <= $i; $j++) {
        $index[$j]['rankl'] = $rankl;
        $index[$j]['rank'] = $rank;
      }
    } else {
      $distinct += 1;
    }
  }
  $rank = array();
  $last = -1;
  $progress = -1;
  for($i = 1; $i <= $count; $i++) {
    if ($last != $index[$i]['rank']) {
      $last = $index[$i]['rank'];
      $progress += 1;
    }
    $fraction = $progress / ($distinct - 1.0);
    $colour = sprintf("#%02x%02x00", floor($fraction * 255), 255-floor($fraction*255));
    $index[$i]['colour'] = $colour;
    $rank[$index[$i]['city']] = $index[$i];
  }
  return $rank;
}

//var_dump(calculate_subcategory_index(array('Wellington', 'Auckland', 'Christchurch', 'Whanganui'), 'Incidence of 3rd World Diseases'))

?>
