<?php

add_shortcode('livability_master', 'shortcode_livability_master');

add_shortcode('livability_city', 'shortcode_livability_master');

add_shortcode('livability_category', 'shortcode_livability_category');

add_shortcode('livability_subcategory', 'shortcode_livability_subcategory');

add_shortcode('livability_indicator', 'shortcode_livability_indicator');

function dump_get($z) {
  return "<pre>".htmlspecialchars(var_export($_GET, TRUE))."</pre>";
}
add_shortcode('dump_get', 'dump_get');
?>
