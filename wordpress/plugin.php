<?php
function shortcode_livability_indicator($z) {
  return "<h1>".htmlspecialchars($_GET['indicator'])."</h1>".shortcode_livability_master($z);
}
function shortcode_livability_category($z) {
  return "<h1>".htmlspecialchars($_GET['single_category'])."</h1>".shortcode_livability_master($z);
}
function shortcode_livability_subcategory($z) {
  return "<h1>".htmlspecialchars($_GET['single_subcategory'])."</h1>".shortcode_livability_master($z);
}

function shortcode_livability_master($z) {
  global $post;
  $page_name = get_post($post)->post_name;
  $city_names = city_names();

  $restrict_to_indicator = null;
  $restrict_to_subcategory = null;
  $restrict_to_category = null;
  $restrict_to_city = null;
  if ($page_name == "master") {
    // OK
  } elseif ($page_name == "category") {
    $restrict_to_category = $_GET['single_category'];
  } elseif ($page_name == "subcategory") {
    $restrict_to_subcategory = $_GET['single_subcategory'];
  } elseif ($page_name == "indicator") {
    $restrict_to_indicator = $_GET['indicator'];
  } else {
    // detect city
    $z = array();
    foreach($city_names as $city) {
      $city_as_slug = str_replace(' ','-',strtolower($city));
      $z[] = array($city_as_slug, $page_name, $city);
      if ($city_as_slug == $page_name) {
        $z[] = array('restrict', $city);
        $restrict_to_city = $city;
      }
    }
    //return ("<pre>".htmlspecialchars(var_export($z, TRUE))."</pre>");

  }

  $missing_cities = array();
  if (isset($_GET['city'])) {
    $cities = $_GET['city'];
    $missing_cities = array_diff($city_names, $cities);
  } else {
    $cities = $city_names;
  }
  //return ("<pre>".htmlspecialchars(var_export(get_page_slug(), TRUE))."</pre>");
  $cities_query = "";
  foreach($cities as $city) {
    if ($cities_query != "") {
      $cities_query .= "&";
    }
    $cities_query .= "city[]=".urlencode($city);
  }
  $category_names = category_names();
  $missing_categories = array();
  if (isset($_GET['category'])) {
    $categories = $_GET['category'];
    $missing_categories = array_diff($category_names, $categories);
  } else {
    $categories = $category_names;
  }
  $categories_query = "";
  foreach($categories as $category) {
    if ($categories_query != "") {
      $categories_query .= "&";
    }
    $categories_query .= "category[]=".urlencode($category);
  }
  if (!is_null($restrict_to_category)) {
    $data = calculate_category_index($cities, $restrict_to_category);
  } elseif (!is_null($restrict_to_subcategory)) {
    $data = calculate_subcategory_index($cities, $restrict_to_subcategory);
  } elseif (!is_null($restrict_to_indicator)) {
    $data = calculate_indicator_index($cities, $restrict_to_indicator);
  } else {
    $data = calculate_index($cities, $categories);
  }
  //return ("<pre>".htmlspecialchars(var_export($data, TRUE))."</pre>");

  $header = "<tr><td>City</td>";
  $header .= "<td>Rank</td>";
  if (!is_null($restrict_to_category)) { // restrict to category
    foreach(subcategory_names($restrict_to_category) as $subcategory) {
      $header .= "<td>";
      $header .= "<a href='/subcategory/?single_subcategory=".urlencode($subcategory)."&$cities_query&$categories_query'>".htmlspecialchars($subcategory)."</a>";
      $header .= "</td>";
    }
    foreach(category_indicator_names($restrict_to_category) as $indicator) {
      $header .= "<td>";
      $header .= "<a href='/indicator/?indicator=".urlencode($indicator)."&$cities_query&$categories_query'>".htmlspecialchars($indicator)."</a>";
      $header .= "</td>";
    }
  } elseif (!is_null($restrict_to_subcategory)) {
    foreach(subcategory_indicator_names($restrict_to_subcategory) as $indicator) {
      $header .= "<td>";
      $header .= "<a href='/indicator/?indicator=".urlencode($indicator)."&$cities_query&$categories_query'>".htmlspecialchars($indicator)."</a>";
      $header .= "</td>";
    }
  } elseif (!is_null($restrict_to_indicator)) {
    $header .= "<td>".htmlspecialchars($restrict_to_indicator)."</td><td>Notes</td>";
  } else { // main index
    foreach($categories as $category) {
      $header .= "<td>";
      $remove_category_query = "";
      foreach($categories as $c) {
        if ($c != $category) {
          if ($remove_category_query != "") {
            $remove_category_query .= "&";
          }
          $remove_category_query .= "category[]=".urlencode($c);
        }
      }
      $header .= "<a href='/category/?single_category=".urlencode($category)."&$cities_query&$categories_query'>".htmlspecialchars($category)."</a>";
      $header .= " <a href='/$page_name/?$cities_query&$remove_category_query'>&Cross;</a>";
      $header .= "</td>";
    }
    if (count($missing_categories) > 0) {
      $header .= "<td><form action='/$page_name/' id='add_cat_form'><select name='category[]' onchange='this.form.submit();'>";
      $header .= "<option selected>Add another category</option>";
      foreach($missing_categories as $category) {
        $header .= "<option>".htmlspecialchars($category)."</option>";
      }
      $header .= "</select>";
      foreach($categories as $category) {
        $header .= "<input type='hidden' name='category[]' value='".htmlspecialchars($category)."'/>";
      }
      foreach($cities as $city) {
        $header .= "<input type='hidden' name='city[]' value='".htmlspecialchars($city)."'/>";
      }
      $header .= "</form></td>";
    }
  }
  $header .= "</tr>";
  $body = "";
  //return ("<pre>RANKED\n".htmlspecialchars(var_export($data['ranked'], TRUE))."</pre>");
  //return ("<pre>RANKED\n".htmlspecialchars(var_export($data, TRUE))."</pre>");

  foreach($data['ranked'] as $x => $row) {
    $city = $row['city'];
    if (is_null($restrict_to_city) || $restrict_to_city == $city) {
      $remove_city_query = "";
      foreach($cities as $c) {
        if ($c != $city) {
          if ($remove_city_query != "") {
            $remove_city_query .= "&";
          }
          $remove_city_query .= "city[]=".urlencode($c);
        }
      }

      $city_a = "<a href='/".urlencode(strtolower($city))."/?$categories_query&$cities_query'>";
      $body .= "<tr><td>".$city_a.htmlspecialchars($city)."</a>";
      if (is_null($restrict_to_city)) {
        $body .= " <a href='/$page_name/?$remove_city_query&categories_query'>&Cross;</a>";
      }
      $body .= "</td>";
      $notes = "";
      $note_text = "";
      foreach($row['notes'] as $note ) {
        if (!is_null($restrict_to_indicator)) {
          $note_text .= htmlspecialchars($note['note']);
        } else {
          $notes .= " <a style='color:black;' href='/indicator/?indicator=".urlencode($note['indicator'])."'>*</a>";
        }
      }
      // don't use notes for first column
      $body .= "<td style='background-color: ".$row['colour']."; color:black;'>".$city_a."<span style='color:black;font-weight:bold;'>".$row['rank']."</span></a></td>";
      if (!is_null($restrict_to_indicator)) {
        $body .= "<td>".$row['value']."</td>";
        $body .= "<td>$note_text</td>";
      } elseif (!is_null($restrict_to_category)) {
        foreach(subcategory_names($restrict_to_category) as $subcategory) {
          $y = $data['by_subcategory'][$subcategory]['ranked'][$city];
          $notes = "";
          foreach($y['notes'] as $note ) {
            $notes .= " <a style='color:black;' href='/indicator/?indicator=".urlencode($note['indicator'])."&cities=$cities_query'>*</a>";
          }
          $body .= "<td style='background-color: ".$y['colour'].";'><span style='color:black; font-weight:bold;'>".$y['rank']."</span></a>$notes</td>";
        }
        foreach(category_indicator_names($restrict_to_category) as $indicator) {
          $y = $data['by_indicator'][$indicator]['ranked'][$city];
          $notes = "";
          foreach($y['notes'] as $note ) {
            $notes .= " <a style='color:black;' href='/indicator/?indicator=".urlencode($note['indicator'])."&cities=$cities_query'>*</a>";
          }
          $body .= "<td style='background-color: ".$y['colour'].";'><span style='color:black; font-weight:bold;'>".$y['rank']."</span></a>$notes</td>";
        }
      } elseif(!is_null($restrict_to_subcategory)) {
        foreach(subcategory_indicator_names($restrict_to_subcategory) as $indicator) {
          $y = $data['by_indicator'][$indicator]['ranked'][$city];
          $note = "";
          foreach($y['notes'] as $note ) {
            $notes .= " <a style='color:black;' href='/indicator/?indicator=".urlencode($note['indicator'])."&cities=$cities_query'>*</a>";
          }
          $body .= "<td style='background-color: ".$y['colour'].";'><span style='color:black; font-weight:bold;'>".$y['rank']."</span></a>$notes</td>";
        }
      } else { // full index
        foreach($data['by_category'] as $category => $category_rank) {
          //return ("<pre>RANKED\n".htmlspecialchars(var_export($ranked, TRUE))."</pre>");

          $y = $category_rank['ranked'][$city];
          $notes = "";
          foreach($y['notes'] as $note) {
            $notes .= " <a style='color:black;' href='/indicator/?indicator=".urlencode($note['indicator'])."&cities=$cities_query'>*</a>";
          }
          $body .= "<td style='background-color: ".$y['colour'].";'><span style='color:black; font-weight:bold;'>".$y['rank']."</span></a>$notes</td>";

        }
      }

      $body .= "</tr>";
    }
  }
  if (count($missing_cities) > 0 && is_null($restrict_to_city)) {
    $body .= "<tr><td><form action='/$page_name/' id='add_city_form'><select name='city[]' onchange='this.form.submit();'>";
    $body .= "<option selected>Add another city</option>";
    foreach($missing_cities as $city) {
      $body .= "<option>".htmlspecialchars($city)."</option>";
    }
    $body .= "</select>";
    foreach($categories as $category) {
      $body .= "<input type='hidden' name='category[]' value='".htmlspecialchars($category)."'/>";
    }
    foreach($cities as $city) {
      $body .= "<input type='hidden' name='city[]' value='".htmlspecialchars($city)."'/>";
    }
    $body .= "</form></td>";
  }
  $body .= "</tr>";
  $result = "<table>$header$body</table>";
  if (!is_null($restrict_to_city)) {
    $result .= "<p><a href='/master/?$cities_query&$categories_query'>Return to full index</a></p>";
  }
  return $result;
}

?>
