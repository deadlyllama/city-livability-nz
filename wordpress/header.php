<?php
/**
 * @package City Livability NZ
 * @version 1.0
 */
/*
Plugin Name: City Livability NZ
Description: Statistics about your town
Author: Donald Gordon / Whangevangelists
Version: 1.0
Author URI: https://2016.hackerspace.govhack.org/content/city-livability-nz
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

?>
