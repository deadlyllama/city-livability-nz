#!/bin/sh

ROOT=`dirname $0`/..

cd $ROOT

mkdir -p transformed/2013-census
cd source-data/2013-census
for i in *.csv
do
  if [ ! -f ../../transformed/2013-census/$i ]; then
    ( head -1 $i ; egrep '^[0-9]{3} ' $i ) | recode ms-ansi..u8  > ../../transformed/2013-census/$i
  fi
done
for i in *.csv
do
  if [ ! -f ../../transformed/2013-census/headings-$i ]; then
    ( for j in `head -1 $i | sed 's/,/ /g'`; do echo $j ; done ) | recode ms-ansi..u8 > ../../transformed/2013-census/headings-$i
  fi
done
cd ../..
python3 transformer/calculator.py
php5 transformer/json2php.php > transformed/data.php

cat wordpress/header.php wordpress/calculate-index.php wordpress/plugin.php wordpress/footer.php transformed/data.php > transformed/plugin.php

php5 run-index.php && \
scp transformed/plugin.php citylivability@10.7.7.3:www/wp-content/plugins/city-livability-nz.php
