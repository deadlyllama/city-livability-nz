#!/usr/bin/python3
import csv
import glob
from pprint import pprint
import json

city = {}
snz_to_city = {}
indicators = {}
categories = set()
rows = []
width = None
with open('data.csv') as f:
    y = 0
    labels = None
    for r in csv.reader(f):
        #pprint(r)
        if width is None:
            width = int((len(r) + 1) / 2) * 2
        for i in range(0, len(r)):
            if r[i] == "":
                r[i] = None
            else:
                r[i] = r[i].strip()
        while len(r) < width:
            r.append(None)
        rows.append(r)
num_indicators = int((width - 2) / 2)

for i in range(0, num_indicators):
    col = i * 2 + 2
    name = rows[0][col]
    order = rows[1][col]
    if order != "SKIP":
        if order != "max":
            order = "min"
        indicators[name] = {
            "name": name,
            "order": order,
            "source": rows[2][col],
            "note": rows[3][col],
            "category": rows[24][col],
            "subcategory": rows[25][col],
            "col": col
            }
for i in range(4, 23):
    name = rows[i][0]
    data = {}
    for n, x in indicators.items():
        v = rows[i][x['col']]
        num = None
        if v.endswith('%'):
            num = float(v[:-1])
        else:
            num = float(v)
        data[n] = {
            "name": n,
            "meta": rows[i][x['col'] + 1],
            "value": num
            }

    city[name] = {
        "name": name,
        "stats-nz-territory": "%03d" % (int(rows[i][1]),),
        "data": data
        }
    snz_to_city[city[name]["stats-nz-territory"]] = name

census_fields = {
    "population": {
        "census-field": "2013_Census_census_usually_resident_population_count(1)",
        "type": "int",
        "category": None,
        "subcategory": None,
        "order": "none"
        }
    }

for f, v in census_fields.items():
    v['name'] = f
    indicators[f] = v

for fn in glob.glob('transformed/2013-census/2013*.csv'):
    with open(fn) as f:
        for row in csv.DictReader(f):
            code = row['Code']
            if code in snz_to_city:
                for f, v in census_fields.items():
                    if v["census-field"] in row:
                        typ = None
                        if v['type'] == 'int':
                            typ = int
                        city[snz_to_city[code]]["data"][f] = {
                            "name": f,
                            "value": typ(row[v["census-field"]]),
                            "meta": None
                            }

with open("transformed/data.json", "w") as f:
    json.dump({
        "city": city,
        "indicators": indicators
        }, f, indent=2)

#for n, i in indicators.items():
#    pprint((n, i))
#pprint(city)
